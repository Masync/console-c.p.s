# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 19:56:50 2020

@author: Juan Manuel Patiño Valencia-
"""
import sys
clients = 'Juan, Manuel, '

def _print_welcome():
    print('Welcome to CPS')
    print('*' * 40)
    print('what would you like to do today?')
    print('[C]reate client')
    print('[R]ead clients')
    print('[U]pdate client')
    print('[D]elete client')
    print('[S]earch client')
    print('*' * 40)
 
    

def create_clients (client_name: str):
    """ create clients in to var {clients} """
    
    global clients
    
    if client_name not in clients:
        clients += client_name
        _add_comma()
   
    else:
        print(f'{client_name} is already exist')
        
def read_clients ():
      """ List clients in to var {clients} """
      global clients
      print(clients)
    
    
def update_client (client_name: str, update_name: str):
    """ update clients in to var {clients} """
    global clients
    
    if client_name in clients:
        clients = clients.replace(client_name +',', update_name + ',')
    else:
        print('client no matched')
        
        
def delete_client (user_name: str):
    """ Delete clients in to var {clients} """
    global clients
    
    if user_name in clients:
        clients = clients.replace(user_name +',', '')
    else:
        print('client no matched')
        
    
def search_clients(user_name: str):
    global clients
    client_list = clients.split(',')
    for client in client_list:
        if client != user_name:
            continue
        else:
            return True

    
def _add_comma():
    global clients
    clients += ','
    
    
def _get_message_client(message:str, option_name:str):
    
    format_message = '*' *20
    print(
        f'{format_message} {option_name.upper()} {format_message}'
        )
    client_name = None
    while not client_name:
      client_name = input(f'{message.capitalize()}: -')
      if client_name == 'exit':
          client_name = None
          break
    if not client_name:
          sys.exit()
     
    return client_name
       
         
if __name__ == '__main__':
    
    _print_welcome()
    command =  input().upper()
    
    if (command == 'C'):
        userData = _get_message_client('insert to new client',
                                       'create client' )
        create_clients(userData)
        read_clients()
    
    elif(command == 'R'):
        read_clients()
        
        
    elif (command == 'U'):
        userData = _get_message_client('name to client',
                                        'update client' )
         
        update_name = input('update name: -').capitalize()
         
        update_client(userData, update_name)
        read_clients()
            
     
    elif (command == 'D'):
       userData = _get_message_client('name to client',
                                      'Delete client')
    
    
    elif (command == 'S'):
        userData = _get_message_client('name to client',
                                        'search client' )
        found = search_clients(userData)
        
        if found:
            print(f'The client {userData} is in the client\'s list')
        else:
            print(f'The client {userData} is not in our client\'s list')
        
        